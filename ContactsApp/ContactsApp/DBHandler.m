//
//  DBHandler.m
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "DBHandler.h"

static DBHandler *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBHandler

+(DBHandler *)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    return sharedInstance;
}

-(BOOL)createAndInitializeDatabase{
    
    NSString *docsDir = NULL;
    BOOL isSuccess = YES;
    
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent:CONTACTS_DATABASE]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath:databasePath] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt ="create table if not exists ContactDetails (id integer primary key, firstname text, middlename text, lastname text, mobilenumber text, homenumber text, email text, address1 text, address2 text)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)!= SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(database);
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

- (BOOL)saveContact:(int)Id firstName: (NSString*)firstName middleName:(NSString*)middleName lastName:(NSString*)lastName mobileNumber:(NSString*)mobileNumber homeNumber:(NSString*)homeNumber email:(NSString *)email adress1:(NSString*)adress1 adress2:(NSString *)adress2
{
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {    NSString *insertSQL = [NSString stringWithFormat:@"insert into ContactDetails (firstname, middlename, lastname, mobilenumber, homenumber, email, address1, address2) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", firstName, middleName, lastName, mobileNumber, homeNumber, email, adress1, adress2];
        const char *insert_stmt = [insertSQL UTF8String];
        
        if(sqlite3_busy_timeout(database, 500) != SQLITE_OK)
        {
                NSLog(@"Error in sqlite3_timeout while inserting: %s",sqlite3_errmsg(database));
        }
        
        if((sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL)) == SQLITE_OK) {
            
            int errCode = sqlite3_step(statement);
            if (errCode != SQLITE_DONE) {
                NSLog(@"Error in sqlite3_step while inserting: %s - error code is %d",sqlite3_errmsg(database), errCode);
                errCode = sqlite3_reset(statement);
                if(errCode != SQLITE_OK) {
                    NSLog( @"Error while reset in insert : %s, error code is %d", sqlite3_errmsg(database), errCode);
                }
                errCode = sqlite3_finalize(statement);
                if(errCode != SQLITE_OK) {
                    NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                errCode = sqlite3_close(database);
                if(errCode != SQLITE_OK) {
                    NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                return NO;
            }
            else
                errCode = sqlite3_finalize(statement);
            if(errCode != SQLITE_OK) {
                NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
            }
            errCode = sqlite3_close(database);
            if(errCode != SQLITE_OK) {
                NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
            }

                return YES;
            
        }
        else {
            NSLog(@"Error while inserting data. %s", sqlite3_errmsg(database));
        }
    }
    
    return NO;
}

-(int) numberOfContacts {
    int numOfRecords = 0;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *sqlStatement = [NSString stringWithFormat: @"select count(*) from ContactDetails"];
        const char *stmt = [sqlStatement UTF8String];
        if((sqlite3_prepare_v2(database, stmt, -1, &statement, NULL)) == SQLITE_OK) {
            if(sqlite3_step(statement) == SQLITE_ROW) {
                numOfRecords = sqlite3_column_int(statement, 0);
            }
            NSLog(@"Number of records is %d",numOfRecords);
        }
        else
        {
            sqlite3_reset(statement);
        }
    }
    int errCode = sqlite3_finalize(statement);
    if(errCode != SQLITE_OK) {
        NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
    }
    errCode = sqlite3_close(database);
    if(errCode != SQLITE_OK) {
        NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
    }

    return numOfRecords;
}

- (NSMutableDictionary *)findContactById:(int)Id
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select * from ContactDetails where id=%d",Id];
        const char *query_stmt = [querySQL UTF8String];
        
        NSMutableDictionary *contactDictionary = [[NSMutableDictionary alloc] init];
        
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *Id = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                [contactDictionary setObject:Id forKey:KEY_ID];
                
                NSString *firstName = [[NSString alloc] initWithUTF8String:(const char*)                        sqlite3_column_text(statement, 1)];
                [contactDictionary setObject:firstName forKey:KEY_FIRSTNAME];
                
                NSString *middleName = [[NSString alloc] initWithUTF8String:(const char *)                        sqlite3_column_text(statement, 2)];
                [contactDictionary setObject:middleName forKey:KEY_MIDDLENAME];
                
                NSString *lastName = [[NSString alloc] initWithUTF8String:(const char *)                        sqlite3_column_text(statement, 3)];
                [contactDictionary setObject:lastName forKey:KEY_LASTNAME];
                
                NSString *mobileNumber = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                [contactDictionary setObject:mobileNumber forKey:KEY_MOBILENUMBER];
                
                NSString *homeNumber = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                [contactDictionary setObject:homeNumber forKey:KEY_HOMENUMBER];
                
                NSString *email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                [contactDictionary setObject:email forKey:KEY_EMAIL];
                
                NSString *address1 = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];
                [contactDictionary setObject:address1 forKey:KEY_ADDRESS1];
                
                NSString *address2 = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                [contactDictionary setObject:address2 forKey:KEY_ADDRESS2];
                
                int errCode = sqlite3_finalize(statement);
                if(errCode != SQLITE_OK) {
                    NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                errCode = sqlite3_close(database);
                if(errCode != SQLITE_OK) {
                    NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }

                return contactDictionary;
            }
            else
            {
                sqlite3_reset(statement);
                int errCode = sqlite3_finalize(statement);
                if(errCode != SQLITE_OK) {
                    NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                errCode = sqlite3_close(database);
                if(errCode != SQLITE_OK) {
                    NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }

                
                return nil;
            }
        }
    }
    return nil;
}

- (BOOL) deleteContactById: (int)Id {
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"delete from ContactDetails where id=%d",Id];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Deletion Successful");
                int errCode = sqlite3_finalize(statement);
                if(errCode != SQLITE_OK) {
                    NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                errCode = sqlite3_close(database);
                if(errCode != SQLITE_OK) {
                    NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
                [self updateContactId:Id];
                return YES;
            }

        }
    }
    int errCode = sqlite3_finalize(statement);
    if(errCode != SQLITE_OK) {
        NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
    }
    errCode = sqlite3_close(database);
    if(errCode != SQLITE_OK) {
        NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
    }
    return NO;
}

- (BOOL) updateContactId: (int)Id {
    
    int numberOfContacts = [self numberOfContacts];
    while (Id <= numberOfContacts) {
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"update ContactDetails set id = %d where id=%d",Id,Id + 1];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            int index = sqlite3_bind_parameter_index(statement, "id");
            
                sqlite3_bind_int64(statement, index, Id+1);
                int errCode = sqlite3_step(statement);
                if (errCode != SQLITE_DONE) {
                    NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
                }
            
            
        }
        int errCode = sqlite3_finalize(statement);
        if(errCode != SQLITE_OK) {
            NSLog( @"Error while finalizing in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
        }
        errCode = sqlite3_close(database);
        if(errCode != SQLITE_OK) {
            NSLog(@"Error while closing database in insert : %s error code is %d", sqlite3_errmsg(database), errCode);
        }
    }
        Id++;
    }
    return YES;
}
@end
