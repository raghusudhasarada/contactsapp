//
//  TableViewController.h
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditContactViewController.h"
#import "DBHandler.h"

@interface TableViewController : UITableViewController

@end
