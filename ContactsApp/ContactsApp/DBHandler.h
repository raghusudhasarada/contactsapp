//
//  DBHandler.h
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

// DATABASE Name
#define CONTACTS_DATABASE @"contacts.db"

// Dictionary key Ids
#define KEY_ID @"kId"
#define KEY_FIRSTNAME @"kFirstName"
#define KEY_MIDDLENAME @"kMiddleName"
#define KEY_LASTNAME @"kLastName"
#define KEY_MOBILENUMBER @"kMobileNumber"
#define KEY_HOMENUMBER @"kHomeNumber"
#define KEY_EMAIL @"kEmail"
#define KEY_ADDRESS1 @"kAddress1"
#define KEY_ADDRESS2 @"kAddress2"

//DBHANDLER INTERFACE
@interface DBHandler : NSObject {
    NSString *databasePath;
}

//CLASS AND INSTANCE METHODS
+(DBHandler *)getSharedInstance;
- (BOOL)createAndInitializeDatabase;
- (BOOL)saveContact:(int)Id firstName: (NSString*)firstName middleName:(NSString*)middleName lastName:(NSString*)lastName mobileNumber:(NSString*)mobileNumber homeNumber:(NSString*)homeNumber email:(NSString *)email adress1:(NSString*)adress1 adress2:(NSString *)adress2;
- (BOOL) deleteContactById: (int)Id;
- (int) numberOfContacts;
- (NSMutableDictionary *)findContactById:(int)Id;
- (BOOL) updateContactId: (int)Id;

@end
