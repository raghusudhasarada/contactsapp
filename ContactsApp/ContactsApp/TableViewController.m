//
//  TableViewController.m
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "TableViewController.h"

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource and delegate methods -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[DBHandler getSharedInstance]numberOfContacts];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = @"contactsData";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellID];
    if(nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSMutableDictionary *data = [[DBHandler getSharedInstance] findContactById:indexPath.row + 1];
    
    cell.textLabel.text = [data objectForKey:KEY_FIRSTNAME];
    cell.detailTextLabel.text = [data objectForKey:KEY_MOBILENUMBER];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[DBHandler getSharedInstance]deleteContactById:indexPath.row + 1 ];
        //[[DBHandler getSharedInstance]updateContactId:indexPath.row + 1];
        [tableView reloadData];
    } else {
        NSLog(@"Unhandled editing style! %d", editingStyle);
    }
    
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *data = [[DBHandler getSharedInstance]findContactById:indexPath.row + 1];
    [self performSegueWithIdentifier:@"accessoryButtonToDetailsSegue" sender:data];
    
}

#pragma Mark - prepare for segue -

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"accessoryButtonToDetailsSegue"]) {
        EditContactViewController *vc = segue.destinationViewController;
        vc.editData = sender;
        NSLog(@"Hi");
    }
    
    else if ([segue.identifier isEqualToString:@"addButtonToDetailsSegue"]) {
        EditContactViewController *vc = segue.destinationViewController;
        vc.Id = [[DBHandler getSharedInstance]numberOfContacts] + 1;
    }
}


@end
