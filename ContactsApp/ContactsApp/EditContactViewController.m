//
//  EditContactViewController.m
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "EditContactViewController.h"


@implementation EditContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    if (!self.Id) {
        self.Id = (int)[self.editData objectForKey:KEY_ID];
    }
    self.firstNameTxtField.text = [self.editData objectForKey:KEY_FIRSTNAME];
    self.middleNameTxtField.text = [self.editData objectForKey:KEY_MIDDLENAME];
    self.lastNameTxtField.text = [self.editData objectForKey:KEY_LASTNAME];
    self.mobileNumberTxtField.text = [self.editData objectForKey:KEY_MOBILENUMBER];
    self.homeNumberTxtFiled.text = [self.editData objectForKey:KEY_HOMENUMBER];
    self.emailTxtField.text = [self.editData objectForKey:KEY_EMAIL];
    self.adress1TxtField.text = [self.editData objectForKey:KEY_ADDRESS1];
    self.address2TxtField.text = [self.editData objectForKey:KEY_ADDRESS2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onPressingSaveButton:(UIBarButtonItem *)sender {
    if(![self isADuplicateContact]) {
        [self saveContact];
    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Duplicate Contact" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

-(void) saveContact {
    if(self.editData == NULL) {
        self.editData = [[NSMutableDictionary alloc]init];
    }
    
    [self.editData setObject:self.firstNameTxtField.text forKey:KEY_FIRSTNAME];
    [self.editData setObject:self.middleNameTxtField.text forKey:KEY_MIDDLENAME];
    [self.editData setObject:self.lastNameTxtField.text forKey:KEY_LASTNAME];
    [self.editData setObject:self.mobileNumberTxtField.text forKey:KEY_MOBILENUMBER];
    [self.editData setObject:self.homeNumberTxtFiled.text forKey:KEY_HOMENUMBER];
    [self.editData setObject:self.emailTxtField.text forKey:KEY_EMAIL];
    [self.editData setObject:self.adress1TxtField.text forKey:KEY_ADDRESS1];
    [self.editData setObject:self.address2TxtField.text forKey:KEY_ADDRESS2];
    
    if (([[self.editData objectForKey:KEY_FIRSTNAME] length] == 0) ||
        ([[self.editData objectForKey:KEY_FIRSTNAME] isEqual : @""])) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NULL message:@"Firstname can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (([[self.editData objectForKey:KEY_LASTNAME] length] == 0) ||
        ([[self.editData objectForKey:KEY_LASTNAME] isEqual: @""])) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NULL message:@"Lastname can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (([[self.editData objectForKey:KEY_MOBILENUMBER] length] == 0) ||
        ([[self.editData objectForKey:KEY_MOBILENUMBER] isEqual: @""])) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NULL message:@"Mobile Number can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (([[self.editData objectForKey:KEY_EMAIL] length] == 0) ||
        ([[self.editData objectForKey:KEY_EMAIL] isEqual: @""])) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NULL message:@"Email Address can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else {
        self.Id = [[DBHandler getSharedInstance]numberOfContacts] + 1;
        BOOL isDataSaved = [[DBHandler getSharedInstance] saveContact:self.Id
                                                            firstName:[self.editData objectForKey:KEY_FIRSTNAME]middleName:[self.editData objectForKey:KEY_MIDDLENAME] lastName:[self.editData objectForKey:KEY_LASTNAME] mobileNumber:[self.editData objectForKey:KEY_MOBILENUMBER] homeNumber:[self.editData objectForKey:KEY_HOMENUMBER] email:[self.editData objectForKey:KEY_EMAIL] adress1:[self.editData objectForKey:KEY_ADDRESS1] adress2:[self.editData objectForKey:KEY_ADDRESS2]];
        
        //show an alert saying contact saved
        if (isDataSaved == YES) {
            NSLog(@"Contact Saved Successfully %d",isDataSaved);
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Contact saved!!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                //pop the screen
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.firstNameTxtField.text = @"";
                self.middleNameTxtField.text = @"";
                self.lastNameTxtField.text = @"";
                self.mobileNumberTxtField.text = @"";
                self.homeNumberTxtFiled.text = @"";
                self.emailTxtField.text = @"";
                self.adress1TxtField.text = @"";
                self.address2TxtField.text = @"";
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else {
            NSLog(@"Contact is not saved %d",isDataSaved);
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Contact not saved!!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                //pop the screen
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.firstNameTxtField.text = @"";
                self.middleNameTxtField.text = @"";
                self.lastNameTxtField.text = @"";
                self.mobileNumberTxtField.text = @"";
                self.homeNumberTxtFiled.text = @"";
                self.emailTxtField.text = @"";
                self.adress1TxtField.text = @"";
                self.address2TxtField.text = @"";
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }

}

-(BOOL) isADuplicateContact {
    if (self.editData) {
        if (([[self.editData objectForKey:KEY_FIRSTNAME] isEqual: self.firstNameTxtField.text]) &&
            ([[self.editData objectForKey:KEY_MIDDLENAME] isEqual: self.middleNameTxtField.text]) &&
            ([[self.editData objectForKey:KEY_LASTNAME] isEqual: self.lastNameTxtField.text]) &&
            ([[self.editData objectForKey:KEY_MOBILENUMBER] isEqual: self.mobileNumberTxtField.text]) &&
            ([[self.editData objectForKey:KEY_HOMENUMBER] isEqual: self.homeNumberTxtFiled.text]) &&
            ([[self.editData objectForKey:KEY_EMAIL] isEqual: self.emailTxtField.text]) &&
            ([[self.editData objectForKey:KEY_ADDRESS1] isEqual: self.adress1TxtField.text]) &&
            ([[self.editData objectForKey:KEY_ADDRESS2] isEqual: self.address2TxtField.text]))
        {
            return YES;
        }
    }
    return NO;
}

@end
