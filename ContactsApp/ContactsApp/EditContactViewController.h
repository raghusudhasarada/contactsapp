//
//  EditContactViewController.h
//  ContactsApp
//
//  Created by Sarada Adhikarla on 11/24/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBHandler.h"
#import "TableViewController.h"

@interface EditContactViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (strong, nonatomic) IBOutlet UITextField *firstNameTxtField;

@property (strong, nonatomic) IBOutlet UITextField *lastNameTxtField;

@property (strong, nonatomic) IBOutlet UITextField *middleNameTxtField;

@property (strong, nonatomic) IBOutlet UITextField *mobileNumberTxtField;

@property (strong, nonatomic) IBOutlet UITextField *homeNumberTxtFiled;

@property (strong, nonatomic) IBOutlet UITextField *emailTxtField;

@property (strong, nonatomic) IBOutlet UITextField *adress1TxtField;

@property (strong, nonatomic) IBOutlet UITextField *address2TxtField;

@property (strong, nonatomic) NSMutableDictionary *editData;

@property int Id;

-(BOOL) isADuplicateContact;

-(void) saveContact;

@end
